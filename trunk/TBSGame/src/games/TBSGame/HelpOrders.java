/*
 * Copyright (c) 2010, Johan T. Larsson All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Mercenary Commander project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *      
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL JOHAN T. LARSSON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package games.TBSGame;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class HelpOrders extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScrollView mainView = new ScrollView(this);

        TextView textView = new TextView(this);
        textView.setText("There are two types of orders that can be given: movement and bombardment orders. The player can switch between them by pressing the order button at the bottom of the screen.\n" +
        		"Orders are given by pressing and holding a unit. After half a second or so, the player will be notified that the unit is being held; drag and drop it onto a location to give it an order.\n" +
        		"To cancel all current orders for a unit, the player drags and drops it onto its own location.\n"+
        		"\n"+
        		"==Movement==\n"+
        		"Movement orders are shown on the battlefield as yellow or red arrows; yellow for movement that will (probably) occur during this turn, and red for movement that will have to wait until the next turn.\n"+
        		"\n"+
        		"==Collisions==\n"+
        		"If something blocks a moving unit, it will wait until the next tick and try again. If two units move onto the same hex at the same tick, precedence is given to the faster unit - unless the two units are on opposing teams, in which case combat will occur.\n"+
        		"\n"+
        		"==Appending Paths==\n"+
        		"To append a movement order to the end of a current path, simply repeat the process. So, if the player wants a unit to go to point A and then continue to point B, he will press and hold the unit, then drop it at point A. He will then repeat the process and drop the unit at point B. The path arrows will indicate a path from the unit to point A and then point B.\n"+
        		"\n"+
        		"==Bombardment==\n"+
        		"Bombardment means that the unit will remain stationary and attack any hostile target it can detect in the selected hex.");
        
        mainView.addView(textView);
        setContentView(mainView);
    }
}
