/*
 * Copyright (c) 2010, Johan T. Larsson All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Mercenary Commander project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *      
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL JOHAN T. LARSSON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package games.TBSGame;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ScrollView;
import android.widget.TextView;

public class HelpCombat extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScrollView mainView = new ScrollView(this);
        
        TextView textView = new TextView(this);
        textView.setText("When two units from opposing teams try to occupy the same hex, combat occurs. Any orders a unit had before the combat will be cleared, which means a travelling unit can be intercepted and interrupted by pursuing units.\n"+
        		"The winner of a combat will remain in the combat hex, while the loser will flee in a random direction. If there is nowhere to flee or its his hitpoints reach 0, the unit is destroyed.\n"+
        		"\n"+
        		"==Offense/Defense==\n"+
        		"Each unit has an offense and a defense value. These are used to determine the outcome of combat. Which value is used depends on the situation:\n" +
        		" * When entering combat without having any orders: Defense\n"+
        		" * When entering combat while having a movement order: Offense\n" +
        		" * When bombarding: Offense\n" +
        		" * When supporting: Offense\n"+
        		"\n"+
        		"==Support==\n"+
        		"Friendly units within firing range of each other will give support during combat. Generally speaking, it is a good idea to surround an enemy and attack. It is a very bad idea to attack a cluster of enemies, unless you are using artillery and firing from beyond the reach of enemy weaponry.");
        mainView.addView(textView);
        setContentView(mainView);
    }
}
