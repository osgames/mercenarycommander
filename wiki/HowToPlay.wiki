#summary How to play the game
#labels Featured,Phase-Requirements

= Introduction =

Although anyone familiar with wargames such as Panzer General should be able to quickly figure out how to play this game, I've realized that the UI is not always immediately obvious to everyone. Therefore, I've created this description. Parts of it will be included in the game itself.

= Turns =

The game is based on simultaneous turns. Each turn has two phases: the interactive phase and the execution phase.

== Interactive Phase ==

During the interactive phase, the player may give his units orders, scan the map, plan his strategies, etc. Nothing done by the player has any immediate effect on the game world.

To zoom in and out on the battlefield, the player uses a pinch gesture or the zoom controls at the bottom of the screen. To scroll the battlefield, he touches the screen with one finger and immediately moves it in any direction. The map will follow the finger of the player.

To check the details of a particular unit or hex, the player briefly taps the screen at the desired unit or hex, then presses the menu button and chooses "selection info".

Once the player is happy with the orders he has given his units, he should press the End Turn button. Once all players have done so, the interactive phase ends and the execution phase begins.

== Execution Phase ==

During the execution phase, each unit performs its given orders. The execution phase is divided into 12 "ticks", and during each tick each unit performs at most one action. So, if a unit has a movement speed of 3 hexes/turn, it would move once every 12/3=4 ticks.

The execution phase is currently instantaneous. There may be a toggle option in the future to have it occur slowly enough for the player to see.

= Orders =

There are two types of orders that can be given: movement and bombardment orders. The player can switch between them by pressing the order button at the bottom of the screen.

Orders are given by pressing and holding a unit. After half a second or so, the player will be notified that the unit is being held; drag and drop it onto a location to give it an order.

To cancel all current orders for a unit, the player drags and drops it onto its own location.

== Movement ==

Movement orders are shown on the battlefield as yellow or red arrows; yellow for movement that will (probably) occur during this turn, and red for movement that will have to wait until the next turn.

=== Collisions ===

If something blocks a moving unit, it will wait until the next tick and try again. If two units move onto the same hex at the same tick, precedence is given to the faster unit - unless the two units are on opposing teams, in which case combat will occur.

=== Appending Paths ===

To append a movement order to the end of a current path, simply repeat the process. So, if the player wants a unit to go to point A and then continue to point B, he will press and hold the unit, then drop it at point A. He will then repeat the process and drop the unit at point B. The path arrows will indicate a path from the unit to point A and then point B.

== Bombardment ==

Bombardment means that the unit will remain stationary and attack any hostile target it can detect in the selected hex.

= Combat =

When two units from opposing teams try to occupy the same hex, combat occurs. Any orders a unit had before the combat will be cleared, which means a travelling unit can be intercepted and interrupted by pursuing units.

The winner of a combat will remain in the combat hex, while the loser will flee in a random direction. If there is nowhere to flee or its his hitpoints reach 0, the unit is destroyed.

== Offense/Defense ==
Each unit has an offense and a defense value. These are used to determine the outcome of combat. Which value is used depends on the situation:
* When entering combat without having any orders: Defense
* When entering combat while having a movement order: Offense
* When bombarding: Offense
* When supporting: Offense

== Support ==
Friendly units within firing range of each other will give support during combat. Generally speaking, it is a good idea to surround an enemy and attack. It is a very bad idea to attack a cluster of enemies, unless you are using artillery and firing from beyond the reach of enemy weaponry.

= Visibility =

= Tile Bonuses =